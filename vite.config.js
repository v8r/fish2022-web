import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import components from 'unplugin-vue-components/vite';
import autoImport from 'unplugin-auto-import/vite';
import { VarletUIResolver } from 'unplugin-vue-components/resolvers';

export default defineConfig({
  // 编译后的baseurl
  base : '/',
  server: {
    // 端口
    port : 3000,
    host: '0.0.0.0',    // 为了能在局域网上用
    // 解决烦人的跨域
    proxy: {
      '/api': {
        target: 'http://154.39.107.215:1027/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  },
  plugins: [
    vue(),
    // 按需引入
    components({
      resolvers: [VarletUIResolver()]
    }),
    autoImport({
      resolvers: [VarletUIResolver({ autoImport: true })]
    })
  ]
})