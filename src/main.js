import { createApp } from 'vue';
import {
  create,
  NInput,
  NCard,
  NButtonGroup,
  NButton,
  NSkeleton,
  NMessageProvider,
  NEmpty
} from 'naive-ui';

const naive = create({
  components: [NInput,NCard,NButtonGroup,NButton,NSkeleton,NMessageProvider,NEmpty]
})


import router from './router';
import './style.css';
import App from './App.vue';

createApp(App).use(naive).use(router).mount('#app');