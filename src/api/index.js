import base from './base';

export async function login(key,password) {
  let r = await base.post('/login',{
    k: key,
    // password: password
  })
  return r;
}
// 获取微信列表
export async function getList() {
  let r = await base.get('/wechat/query');
  return r;
}
// 获取二维码
export async function getCode(key) {
    let r = await base.get(`/wechat/add?k=${key}`);
    if (!r) {
      message.info('身份失效！');
    }
    return r;
}
// 检测二维码
export async function isCodeYes(uuid) {
  let r = await base.get(`wechat/check?uuid=${uuid}`);
  return r;
}
// 唤醒
export async function Awaken(wxid) {
  let r = await base.get(`/wechat/awaken?k=${wxid}`);
  return r;
}
// 二次取码
export async function GetCodeTwo(wxid) {
  let r = await base.get(`/wechat/secondary?k=${wxid}`);
  return r;
}
// 解绑
export async function Unbinding(key) {
  let r = await base.get(`/wechat/unbinding?k=${key}`);
  return r;
}
// 续费
export async function Renew(key,wxid) {
  let r = await base.post('/wechat/renew',{
    k: key,
    wxid
  })
  return r;
}
// 获取配置信息
export async function getSettings(wxid) {
  let r = await base.get(`/wechat/config/query?wxid=${wxid}`)
  return r;
}
// 更新配置
export async function updateSettings(wxid,setings) {
  let r = await base.post('/wechat/config/update',{
    wxid,
    ...setings
  })
  return r;
}