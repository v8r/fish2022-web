import {createRouter, createWebHistory } from 'vue-router';

const routes = [
	{
      path: '/',
      redirect: '/home'
	},{
		path: '/home',
		component: () => import('@/views/Home.vue')
	},{
    name: 'authorization',
		path: '/authorization',
		component: () => import('@/views/Authorization.vue')
	},{
		path: '/settings/:uid',
		component: () => import('@/views/Settings.vue')
	},
  // 设置朋友圈
  {
		path: '/details_comment/:wxid',
		component: () => import('@/views/DetailsComment.vue')
	},
  // 朋友圈转发
  {
		path: '/details_reprint/:wxid',
		component: () => import('@/views/DetailsReprint.vue')
	},
  // 自动通过好友
  {
		path: '/details_transit/:wxid',
		component: () => import('@/views/DetailsTransit.vue')
	},
  {
		path: '/details_reply/:wxid',
		component: () => import('@/views/DetailsReply.vue')
	},
]

const router = new createRouter({
	history: createWebHistory(),
	routes
})

export default router