export function setToken(value) {
  localStorage.setItem('VmtjNWNscFhOQT09', value);
  return localStorage.getItem('VmtjNWNscFhOQT09') ?? false;
}

export function getToken() {
  let value = localStorage.getItem('VmtjNWNscFhOQT09');
  return value ?? '';
}

export function logout() {
  return localStorage.removeItem('VmtjNWNscFhOQT09') ?? false;
}